/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.thatchai.swingproject;

/**
 *
 * @author Windows10
 */
public class TestException {

    public static void main(String[] args) {
        try {
          throw new Exception("Hello Exception");
            //t[] arr = {1, 2, 3};
            //stem.out.println(arr[0]);
            //stem.out.println(arr[1]);
            //stem.out.println(arr[2]);
            //stem.out.println(arr[3]);
            //stem.out.println("Hello");
             //teger.parseInt("-");
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Array Exception");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }finally{
            System.out.println("Final Statement");
        }
    }
}
